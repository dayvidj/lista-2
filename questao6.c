#include <stdio.h>
//Dayvid Jonathan 
int main() {
    int tamanho, razao, resultado = 1, cont = 0;
    printf("Digite a quantidade de elementos: "); 
    scanf("%d", &tamanho);
    printf("Razao: ");
    scanf("%d", &razao);
    while(cont < tamanho){
        printf("%d ", resultado);
        resultado *= razao;
        cont++;
    }
    return 0;
}
